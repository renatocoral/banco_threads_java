public class ContaCorrente {
	private double saldo;
	private double temp;
	
	public ContaCorrente(double saldo) {
		this.saldo = saldo;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public synchronized void depositar(double valor) {
		temp = getSaldo();
		saldo = temp + valor;
	}
	
	public synchronized boolean sacar(double valor) {
		temp = getSaldo();
		if(temp < valor)
				return false;
		else {
			saldo = temp - valor;
			return true;
		}
	}
}
